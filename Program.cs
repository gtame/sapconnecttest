﻿using System;
using System.Collections.Generic;
using System.Text;
using SAP.Middleware.Connector;

namespace SAPConnectionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Introduzca codigo de compañia:");
            string company = System.Console.ReadLine();
            string name = GetCompanyName(company);
            System.Console.WriteLine(string.Format("Nombre Compañia: {0}", name));
            System.Console.ReadKey();
        }


        public static string GetCompanyName(string companyID)
        {
            string companyName = null;

                //Obtenemos la conexion definida en el config
                RfcDestination _destinationSAP = RfcDestinationManager.GetDestination("SAP");
                //Inicializamos el contexto de la llamada
                RfcSessionManager.BeginContext(_destinationSAP);
                //Obtenemos la funcion
                IRfcFunction companyAPI = _destinationSAP.Repository.CreateFunction("BAPI_COMPANY_GETDETAIL");
                //Fijamos el parametro
                companyAPI.SetValue("COMPANYID", companyID);
                //Invocamos la funcion
                companyAPI.Invoke(_destinationSAP);
                //Obtenemos la estructura que devuelve la funcion
                IRfcStructure _companyDetail = companyAPI.GetStructure("COMPANY_DETAIL");
                //Obtenemos el campo NAME
                companyName = _companyDetail.GetString("NAME1");
                //Si es null lanzamos excepcion
                if (string.IsNullOrEmpty(companyName)) throw new Exception(string.Format("No se encontro la compañia con codigo {0}", companyID));
                //Finalizamos el contexto de la llamada
                RfcSessionManager.EndContext(_destinationSAP);

       

            return companyName;
        }
    }
}
